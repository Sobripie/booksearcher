export function getDescriptiveErrMsg(err: unknown, prefix: string): string {
  let message = prefix;
  if (err instanceof Error) {
    message += err.message;
  }
  else {
    message += "unknown error";
  }
  return message;
}