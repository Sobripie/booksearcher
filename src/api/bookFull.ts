import { API_URL_FULLVOLUME } from "src/_CONSTANTS/general";
export function getFetchFullVolumeResponse(id: string): Promise<Response> {
  const requestUrl = `${API_URL_FULLVOLUME}${id}`;
  return fetch(requestUrl);
}