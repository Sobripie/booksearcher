import { useDispatch } from "react-redux";
import { useBookPreviews } from "src/features/bookPreviews/bookPreviewsSlice";
import { fetchLoadMoreBooks } from "src/features/bookPreviews/fetchLoadMoreBooks";
import { BookPreview } from "src/types/BookPreview";
import { AsyncButton } from "src/components/AsyncButton";
import { BookPreviewComponent } from "src/components/BookPreviewComponent";

import STYLES from "./IndexPage.module.scss";
import { FoundInfo, InfoScrolled } from "src/components/FoundInfo";
import { useEffect, useRef, useState } from "react";
import { useCurrentBook } from "src/features/currentBook/currentBookSlice";
import { useNavigation } from "src/features/navigation/navigationSlice";

export const IndexPage = () => {
  const dispatch = useDispatch();
  const { status, error, maxResults,
    items: {
      foundCount,
      list,
      currentCategory,
      currentOrder,
      currentQuery
    }
  } = useBookPreviews();
  const { cachedBooks } = useCurrentBook();
  const { memoScroll }= useNavigation();


  const [isScrolled, setIsScrolled] = useState<boolean>(false);
  const intersectionRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const observedElem = intersectionRef.current;
    if (!observedElem) return;

    const io = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (!entry.isIntersecting) {
          setIsScrolled(true);
        }
        else {
          setIsScrolled(false);
        }
      });
    });

    io.observe(observedElem);

    window.scroll(0, memoScroll["previews"]);

    return () => {
      io.unobserve(observedElem);
    }
  }, [memoScroll]);

  const loadMore = () => {
    dispatch(fetchLoadMoreBooks({
      "category": currentCategory,
      "order": currentOrder,
      "query": currentQuery,
      "startIndex": list.length,
      "maxResults": maxResults
    }));
  }

  const renderLoadMoreButton = (): JSX.Element | undefined => {
    if (list.length === 0 || list.length === foundCount) {
      return;
    }

    return (
      <AsyncButton 
        isLoading={status === "loading"}
        error={!!error}
        className={{
          "idle": STYLES.loadMoreButton,
          "error": `${STYLES.loadMoreButton} ${STYLES.error}`,
          "loading": `${STYLES.loadMoreButton} ${STYLES.loading}`,
          "success": `${STYLES.loadMoreButton} ${STYLES.success}`,
        }}
        iconClass={{
          idle: "icon-down-open",
          error: "icon-cw",
          loading: "icon-arrows-cw spin",
          success: "icon-ok"
        }}
        text={{
          idle: "Load more",
          error: "Try again?",
          loading: "Loading...",
          success: "Loaded"
        }}
        onClick={loadMore}
      />
    )
  }

  return (
    <div className={STYLES.container}>
      <FoundInfo ref={intersectionRef}/>
      <InfoScrolled isVisible={isScrolled}/>
      <div className={STYLES.previews}>
        {
          list.map((bookPreviewData: BookPreview, i: number) => {
            const {id}= bookPreviewData;
            let isVisited = false;
            if (cachedBooks[id]) {
              isVisited = true;
            }
            return (
              <BookPreviewComponent 
                key={i}
                isVisited={isVisited}
                bookPreviewData={bookPreviewData}
              />
            ); 
          })
        }     
      </div>
      {
        renderLoadMoreButton()
      }
    </div>
  )
}