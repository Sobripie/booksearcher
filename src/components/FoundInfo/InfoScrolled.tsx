import { useCallback } from "react";
import { useBookPreviews } from "src/features/bookPreviews/bookPreviewsSlice";
import STYLES from "./InfoScrolled.module.scss";

interface Props_ShortInfoItem {
  iconClass: string;
  text: string;
  hint: string;
}
const ShortInfoItem = ({iconClass, text, hint}: Props_ShortInfoItem): JSX.Element => {
  return (
    <div className={STYLES.shortInfoItem} title={hint}> 
      <i className={iconClass} />
      <span className={STYLES.shortInfoText}>{text}</span>
    </div>
  )
}

interface Props_InfoScrolled {
  isVisible?: boolean;
}

/** Фиксед шапка */
export const InfoScrolled = ({isVisible}: Props_InfoScrolled) => {
  const { status, error,
    items: {
      foundCount,
      list,
      currentCategory,
      currentOrder,
      currentQuery
    }
  } = useBookPreviews();

  const memoRenderStatus = useCallback((): JSX.Element | undefined => {
    if (status === "loading") {
      return (
        <div className={STYLES.shortInfoItem + " " + STYLES.loading}>
          <i className="icon-arrows-cw spin" />
          <span>Loading...</span>
        </div>
      )
    }
    if (error) {
      return (
        <div className={STYLES.error} title={"Error: " + error}>
          {
            "Error: " + (error.length < 16 ? error : error.slice(0, 13).trim() + "...")
          }
        </div>
      )
    }
  }, [status, error]);

  const memoGetShortInfoItems = useCallback(():Props_ShortInfoItem[] => {
    return [
      {
        iconClass: "icon-book",
        text: list.length + " / " + foundCount,
        hint: "Found books: showing / total"
      },
      {
        iconClass: "icon-search",
        text: currentQuery.length < 16
          ? currentQuery
          : currentQuery.slice(0, 13).replace(/\s+$/, "") + "...",
        hint: "You searched for: " + currentQuery
      },
      {
        iconClass: "icon-tags",
        text: currentCategory,
        hint: "Category: " + currentCategory
      },
      {
        iconClass: "icon-sort-alt-up",
        text: currentOrder,
        hint: "Order by: " + currentOrder
      },
    ]
  }, 
  [foundCount, currentQuery, currentCategory, currentOrder, list.length]);

  const memoRenderShortInfoItems = useCallback((): JSX.Element | undefined => {
    if (list.length === 0) return;
    return (
      <>
        {
          memoGetShortInfoItems().map((props: Props_ShortInfoItem, i: number) => (
            <ShortInfoItem 
              key={i}
              {...props}
            />
          ))
        }
      </>
    )
  }, [list.length, memoGetShortInfoItems]);

  return (
    <div className={`${STYLES.fixedPopup}${isVisible ? " " + STYLES.shown : ""}`}>
      {
        memoRenderStatus()
      }
      {
        memoRenderShortInfoItems()
      }
      
      <button 
        className={STYLES.goTopBtn}
        onClick={() => window.scrollTo(0, 0)}
      >
        <i className={"icon-down-open " + STYLES.rot180} />
        <span>Go top</span>
      </button>
    </div>
  )
}