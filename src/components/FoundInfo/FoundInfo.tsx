import { forwardRef, useCallback } from "react";
import { useBookPreviews } from "src/features/bookPreviews/bookPreviewsSlice";
import STYLES from "./FoundInfo.module.scss";

export const FoundInfo = forwardRef((_, ref: React.LegacyRef<HTMLDivElement>) => {
  const { status, error,
    items: {
      foundCount,
      list,
      currentCategory,
      currentOrder,
      currentQuery
    }
  } = useBookPreviews();

  const memoRenderCurrentInfo = useCallback((): JSX.Element | undefined => {
    if (list.length === 0) {
      if (status === "idle") {
        return <h3 
          className={STYLES.foundInfo}
          title="Бесплатный хостинг Heroku выключает процесс после 30 минут неактивности, а когда получает реквест - запускает процесс. Это должно занять не более пяти секунд."
        >
          You'll <b className={STYLES.warningAccent}>probably</b> have to wait for a few seconds <b className={STYLES.warningAccent}>after the first search</b>, while my backend on a free Heroku hosting is warming up.
        </h3>;
      }
      return;
    }

    const foundString = `Found ${foundCount} books in category "${currentCategory}", ordered by: ${currentOrder}`;
    const queryString = currentQuery.length < 33 
      ? currentQuery
      : currentQuery.slice(0, 32).replace(/\s+$/, "") + "..."
    ;
    const showingString = `Showing ${list.length}`;

    return (
      <>
        <h3 className={STYLES.foundInfo}>
        {foundString}
        </h3>
        <span className={STYLES.query}>{`On request: "${queryString}"`}</span>
        <span className={STYLES.showing}>{showingString}</span>
      </>
    )
  }, [foundCount, currentCategory, currentOrder, currentQuery, list.length, status]);

  const memoRenderStatus = useCallback((): JSX.Element | undefined => {
    if (status === "loading") {
      return (
        <div className={STYLES.foundLoading}>
          <i className="icon-arrows-cw spin" />
          <span>Loading...</span>
        </div>
      )
    }
    if (error) {
      return (
        <h3 className={`${STYLES.foundInfo} ${STYLES.foundError}`}>
          {`Failed to fetch. Reason: ${error}`}
        </h3>
      )
    }
  }, [status, error]);
  
  return (
    <div 
      className={STYLES.foundInfoContainer}
      ref={ref}
    >
      {
        memoRenderStatus()
      }
      {
        memoRenderCurrentInfo()
      }
    </div>
  )
});