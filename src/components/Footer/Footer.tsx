import { FunctionComponent, useCallback, useState } from "react";
import STYLES from "./Footer.module.scss";

interface Props_LinkPlain {
  href: string;
  text: string;
}
const LinkPlain = ({href, text}: Props_LinkPlain): JSX.Element => {
  return (
    <a
      className={STYLES.footerLink}
      href={href}
      target="_blank"
      rel="noreferrer"
    >
      {text}
    </a>
  )
}

interface Props_LinkWithIcon {
  iconClass: string;
  text: string;
  href: string;

}
const LinkWithIcon = ({iconClass, text, href}: Props_LinkWithIcon): JSX.Element => {
  return (
    <a 
      className={STYLES.sourceItem}
      href={href}
      target="_blank"
      rel="noreferrer"
    >
      <i className={iconClass} />
      <span>{text}</span>
    </a>
  )
}

export const Footer: FunctionComponent = () => {

  const [isCopied, setIsCopied] = useState<boolean>(false);

  const email = "sobripie@yandex.ru"
  const memoCopyText = useCallback(async () => {
    await navigator.clipboard.writeText(email);
    if (isCopied) return;

    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 3000)
  }, [isCopied]);
  const memoGetCopyItemClassName= useCallback((): string => {
    let className = STYLES.sourceItem;
    if (isCopied) {
      className += " " + STYLES.copied;
    }
    return className;
  }, [isCopied]);

  return (
    <footer className={STYLES.container}>
      <div className={STYLES.left}>
        <div className={STYLES.title}>
          <i className={"icon-child " + STYLES.icon} />
          <h4 className={STYLES.booksearcherBy}>Booksearcher by</h4>
          <div className={STYLES.name}>Денис Сагитов</div>
          <div className={STYLES.poweredBy}>
            Powered by
            <LinkPlain 
              href="https://docs.gitlab.com/ee/user/project/pages/"
              text="Gitlab Pages"
            />
            /
            <LinkPlain 
              href="https://devcenter.heroku.com/articles/getting-started-with-nodejs"
              text="Heroku"
            />
            /
            <LinkPlain 
              href="https://create-react-app.dev/"
              text="CRA"
            />
            
          </div>
        </div>

        <div className={STYLES.technologies}>
          <h4>Techonologies &amp; frameworks</h4>
          <p>Typescript, React, Redux Toolkit, Sass,  Fontello</p>
          <p>Express, Docker, Nginx</p>
        </div>
      </div>

      <div className={STYLES.right}>
        <div className={STYLES.footerList + " " + STYLES.sources}>
          <h4>Source</h4>
          <LinkWithIcon 
            iconClass="icon-gitlab"
            text="Simple backend"
            href="https://gitlab.com/Sobripie/simple-backend"
          />
          <LinkWithIcon 
            iconClass="icon-gitlab"
            text="Frontend"
            href="https://gitlab.com/Sobripie/booksearcher"
          />
        </div>
        
        <div className={STYLES.footerList + " " + STYLES.contacts}>
          <h4>Contacts</h4>
          <div
            className={memoGetCopyItemClassName()}
            onClick={memoCopyText}
          >
            <i className="icon-mail" />
            <span>sobripie@yandex.ru</span>
          </div>
          <LinkWithIcon 
            iconClass="icon-github-circled"
            text="Sobriquetique"
            href="https://github.com/Sobriquetique"
          />
          <LinkWithIcon 
            iconClass="icon-gitlab"
            text="@Sobripie"
            href="https://gitlab.com/Sobripie"
          />
        </div>
      </div>
      
    </footer>
  )
};