import { createAsyncThunk } from "@reduxjs/toolkit";
import { getBookVolumesResponse } from "src/api/bookPreviews";
import { CategoryName } from "src/types/Category";
import { GoogleAPIBookVolumes } from "src/types/GoogleAPI";
import { OrderByName } from "src/types/OrderByName";
import { getDescriptiveErrMsg } from "src/utils/getDescriptiveErrMsg";
import { mapAPIPreviewsToLocal } from "src/utils/mapAPIToLocal";
import { FetchBookPreviewsThunk_Return } from "./bookPreviewsSlice";

interface ThunkArgs {
  query: string;
  category: CategoryName;
  order: OrderByName;

  /** Добавляем возможность передавать из стейта на случай, если захотим настраивать пагинацию динамически */
  maxResults?: number;
}

/** Фетчит книги с нуля. Обнулить current list книг и засунуть туда этот в экстра редусерах. */
export const fetchFreshBookPreviews = createAsyncThunk<FetchBookPreviewsThunk_Return, ThunkArgs, { rejectValue: string }>(
  "bookPreviews/fetch",
  async ({query, category, order, maxResults}: ThunkArgs, {rejectWithValue}) => {
    try {
      const response = await getBookVolumesResponse({
        query, category, order, startIndex: 0, maxResults
      });
      if (!response.ok) {
        const errorResponseBody = await response.json();
        console.log(errorResponseBody);
        return rejectWithValue(`Server responded with ${response.status}`);
      }

      const data: GoogleAPIBookVolumes = await response.json();

      if (!("items" in data) || !Array.isArray(data.items)) {
        return rejectWithValue(`No books found with query "${query}"`);
      } 

      return {
        list: mapAPIPreviewsToLocal(data.items),
        foundCount: data.totalItems,
        newCategory: category,
        newOrder: order,
        newQuery: query
      }
      
    }
    catch (err) {
      return rejectWithValue(getDescriptiveErrMsg(err, ""));
    }
  }
)