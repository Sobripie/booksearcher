## Учебный проект booksearcher

Упрощенный просмотр книг с использованием публичного API Google Books.

https://sobripie.gitlab.io/booksearcher/

### Использованные технологии и фреймворки

- Typescript
- React
- Redux Toolkit
- SASS
- Docker (local production build)
- Nginx (local production build)
- Create-react-app
- Fontello (иконки)

**Задеплоено на Gitlab Pages**

Простенький бекенд для скрытия приватного ключа Google API:

https://gitlab.com/Sobripie/simple-backend

Задеплоен на heroku

### Как поднять production билд у себя

- Клоним репозиторий и заходим в созданную директорию
```
git clone https://gitlab.com/Sobripie/booksearcher.git
cd booksearcher
```

- Открываем `./nginx.conf`, на третьей строчке находим дефолтный порт. Меняем, если этот у нас уже занят
```
listen 10123
```

- Собираем docker image
```
docker build -t your-image-name .
```

- Запускаем контейнер
```
docker run --name your-container-name -d -p 10123 your-image-name
```
**Если меняли порт в `./nginx.conf`, пишем здесь тот же.**

- Открываем в браузере localhost:10123

### Как развернуть проект для разработки (dev)

- Если еще не сделали, клоним репозиторий и заходим в созданную директорию
```
git clone https://gitlab.com/Sobripie/booksearcher.git
cd booksearcher
```

- Устанавливаем нпм пакеты и запускаем дев-сервер create-react-app. Ждем, пока автоматически откроется localhost:3000 в браузере.
```
npm install
npm run start
```

### Пара нюансов

- Оказывается, обращаться к апи можно без API ключа, несмотря на то, что в доках Google API об этом ничего не сказано. Но это приложение все равно шлет запросы с ключом.

- На некоторые книги (не превью, а по эндпоинту volumes/${id}) гугл апи выдает 503 Service unavailable, это нормально. Проверял curl'ом с ключом или без ключа, и они действительно недоступны. Просто жмем на кнопку Go back под шапкой и пробуем другие книги.

- В качестве изображений для полного просмотра книги выбрал thumbnail'ы вместо больших изображений, потому что в 90% случаев там мусор или плейсхолдеры, а актуальные thumbnail'ы есть почти всегда.
